import React, { useState, useEffect, useContext } from 'react';
/*react-bootstrap component*/
import { Container, Row } from 'react-bootstrap'

/*components*/
// import Product from './../components/Product';
import CartView from './../components/CartView.js';

/*context*/
import UserContext from './../UserContext';

export default function Cart(){

	const [orders, setOrders] = useState([]);

	const {user} = useContext(UserContext);

	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('http://localhost:4000/users/getOrders',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log("result")
			console.log(result)
			setOrders(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

	// let CourseCards = courses.map( (course) => {
	// 	return <Course key={course.id} course={course}/>
	// })
 
	return(
		<Container className="p-4">
				<CartView orderData={orders} fetchData={fetchData}/>
		</Container>
	)
}