// Capstone 3
import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const {user} = useContext(UserContext);

	const navigate = useNavigate()

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState(undefined);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				email : email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {
				Swal.fire({
					title : "Email Already Exists",
					icon : "error",
					text : "Please provide another email."
				})

			} else {
				fetch('http://localhost:4000/users/register', {
					method : 'POST',
					headers : {
						'Content-Type' : 'application/json'
					},
					body : JSON.stringify({
						firstName : firstName,
						lastName : lastName,
						mobileNo : mobileNo,
						email : email,
						password : password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data === true){
						// clear input fields
						setFirstName('');
						setLastName('');
						setMobileNo(undefined);
						setEmail('');
						setPassword('');

						Swal.fire({
							title : "Registration Successful!",
							icon : "success",
							text : "Thank you for registering!"
						});

						navigate("/login");
					} else {
						Swal.fire({
							title : "Something went wrong.",
							icon : "error",
							text : "Please try again."
						});

					}
				})
			}
		})
	};

	useEffect(() => {

		if (firstName !== '' && lastName !== '' && email !== '' && password !== '') {
				setIsActive(true);

			} else {
				setIsActive(false);
			};

		}, [firstName, lastName, mobileNo, email, password])

	return(

		(user.id !== null) ?

		<Navigate to="/products"/>

		:

		<>
		<Form onSubmit={e => registerUser(e)}>
			<h1 className="my-3 text-center">Register</h1>

			<Container className="border border-2 rounded-top">

				<Form.Group controlId="firstName" className="my-3">
					<Form.Label>First Name</Form.Label>
					<Form.Control
						type = "text"
						placeholder = "Input your First Name here."
						value = {firstName}
						onChange = {e => setFirstName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="lastName" className="mb-3">
					<Form.Label>Last Name</Form.Label>
					<Form.Control
						type = "text"
						placeholder = "Input your Last Name here."
						value = {lastName}
						onChange = {e => setLastName(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="mobileNo" className="mb-3">
					<Form.Label>Mobile Number (Optional)</Form.Label>
					<Form.Control
						type = "text"
						placeholder = "Input your Mobile Number here."
						value = {mobileNo}
						onChange = {e => setMobileNo(e.target.value)}
					/>
				</Form.Group>

				<Form.Group controlId="userEmail" className="my-3">
					<Form.Label>Email:</Form.Label>
					<Form.Control
						type = "email"
						placeholder = "Enter your email."
						value = {email}
						onChange = {e => setEmail(e.target.value)}
						required
					/>
				</Form.Group>

				<Form.Group controlId="password" className="my-3">
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type = "password"
						placeholder = "Enter your password."
						value = {password}
						onChange = {e => setPassword(e.target.value)}
						required
					/>
				</Form.Group>

			</Container>

			<Container className="border border-top-0 border-2 rounded-bottom">
				{ isActive ?
					<Button variant="success" type="submit" id="submitBtn" className="my-3 w-100">
						Submit
					</Button>
					:
					<Button variant="danger" type="submit" id="submitBtn" className="my-3 w-100" disabled>
						Please enter your registration details.
					</Button>
				}
			</Container>
		</Form>

		<p className="text-center my-3">
			Already have an account? <Link to="/login" className="text-decoration-none">Click here</Link> to log in.
		</p>
		</>
	);
};