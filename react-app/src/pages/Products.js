import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

import React, { useState, useEffect, useContext } from 'react';
/*react-bootstrap component*/
import { Container, Row } from 'react-bootstrap'

/*components*/
// import Product from './../components/Product';
import AdminView from './../components/AdminView.js';
import UserView from './../components/UserView.js';


/*mock data*/
// import courses from './../mock-data/courses';

/*context*/
import UserContext from './../UserContext';

export default function Products(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	const fetchData = () => {
		let token = localStorage.getItem('token')

		fetch('http://localhost:4000/products/all',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log("result")
			console.log(result)
			setProducts(result)
		})
	}

	useEffect( () => {
		fetchData()
	}, [])

	// let CourseCards = courses.map( (course) => {
	// 	return <Course key={course.id} course={course}/>
	// })
 
	return(
		<>
			<Banner/>
			<Highlights/>
			<hr className="mt-5"/>
			<Container className="p-4">
				{ (user.isAdmin === true) ?
					<AdminView productData={products} fetchData={fetchData}/>
					:
					<>
						<h1 className="mt-3 text-center">Our Products</h1>
						<Row className="my-3">
							<UserView productData={products} />
						</Row>
					</>
				}
			</Container>
		</>
	)
}



// import { useState, useEffect } from 'react';
// import { Row, Col } from 'react-bootstrap';

// import ProductCard from '../components/ProductCard';
// import UserView from './../components/UserView.js';

// export default function Products(){

// 	const [products, setProducts] = useState([]);

// 	useEffect(() => {
// 		fetch('http://localhost:4000/products')
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data)

// 			setProducts(data.map(product => {
// 				return (
// 					<Col xs={12} md={6} lg={4} className="my-3">
// 						<ProductCard key={product._id} productProp={product}/>
// 					</Col>
// 				);
// 			}));
// 		})
// 	}, [])

// 	return(
// 		<>
// 			<h1 className="mt-3 text-center">Our Products</h1>
// 			<Row className="my-3">
// 				{products}
// 			</Row>
// 		</>
// 	)
// };
