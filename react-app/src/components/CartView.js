// // import { useEffect, useState } from 'react';
// // import { Row, Col } from 'react-bootstrap';


// // export default function CartView(orderProp){

// // // 	const {}

// // // 	const [orders, setOrders] = useState([]);

// // // 	useEffect(() => {
// // // 		fetch('http://localhost:4000/getOrders')
// // // 		.then(res => res.json())
// // // 		.then(data => {
// // // 			console.log(data)

// // // 			setOrders(data.map(order => {
// // // 				return (
// // // 					<tr>
// // // 						<td></td>

// // // 					</tr>
// // // 				)
// // // 			}))

// // // 		})
// // // 	})

// // 	return(
// // 		<>
// // 			<h1 className="my-3 text-center">Your Cart</h1>
// // 				<table className="mx-auto">
// // 					<tr className="bg-dark text-white">
// // 						<th className="px-5">Name</th>
// // 						<th className="px-5">Price</th>
// // 						<th className="px-5">Quantity</th>
// // 						<th className="px-5">Subtotal</th>
// // 					</tr>
					
// // 				</table>
// // 		</>
// // 	)
// // }


import { Fragment, useState, useEffect, useContext } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CartView(props){

	let totalCarrier = 0
	let subtotal = 0

	const { orderData, fetchData } = props

	const [orderId, setOrderId] = useState([]);
	const [orders, setOrders] = useState([]);
	const [productPrice, setProductPrice] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);
	const [total, setTotal] = useState(0)


	const cancelOrder = (orderId, orderStatus) => {

		console.log(orderStatus);

		fetch(`http://localhost:4000/users/${ orderId }/cancel`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				orderStatus: "Cancelled"
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {
				console.log(data)

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Order successfully cancelled."
				});

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}
		})
	}

	const purchaseOrder = (orderId, orderStatus) => {

		console.log(orderStatus);

		fetch(`http://localhost:4000/users/${ orderId }/purchase`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				orderStatus: "Purchased"
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {
				console.log(data)

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Order(s) successfully purchased."
				});

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}
		})
	}

	useEffect(() => {

		const ordersArr = orderData.map(order => {

			console.log(order.totalAmount)
			console.log(order.productPrice)
			totalCarrier += (order.totalAmount * order.productPrice)
			subtotal = (order.totalAmount * order.productPrice)
			console.log(totalCarrier)
			console.log("stop")


			return(

				<tr key={order._id}>
					<td>{order.productName}</td>
					<td>PhP {order.productPrice}</td>
					<td className="text-center">{order.totalAmount}</td>
					<td>PhP {subtotal}</td>
					<td>
					<Button 
						className="w-100 mb-2"
						variant="danger" 
						size="sm" 
						onClick={() => cancelOrder(order._id, order.orderStatus)}
					>
						Remove
					</Button>
					<Button 
						className="w-100"
						variant="success" 
						size="sm" 
						onClick={() => purchaseOrder(order._id, order.orderStatus)}
					>
						Purchase
					</Button>
					</td>
				</tr>
			)
		})

		setOrders(ordersArr)
		setTotal(totalCarrier)

	}, [orderData, fetchData])

	return(
		<Fragment>
			<h1 className=" text-center mb-3">Cart</h1>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Subtotal</th>
						<th></th>
					</tr>					
				</thead>
				<tbody>
					{orders}
					<tr>
						<th colSpan="3">
							<Link className="btn btn-success w-100" to="/">Back to Store</Link>
						</th>
						<th colSpan="2" className="text-center">Total: {total}</th>
					</tr>
				</tbody>
	
			</Table>
		</Fragment>
	)
}