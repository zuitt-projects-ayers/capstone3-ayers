import React, {useState, useEffect} from 'react'

import { Container, Row, Col } from 'react-bootstrap'

import Products from '../pages/Products'

import ProductCard from '../components/ProductCard';

export default function UserView({productData}){

	console.log("productData")
	console.log(productData)

	const [product, setProducts] = useState([])

	useEffect(() => {
		fetch('http://localhost:4000/products')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return (
					<Col xs={12} md={6} lg={4} className="my-3" key={product._id}>
						<ProductCard productProp={product}/>
					</Col>
				);
			}));
		})
	}, [])

	return (
		<>
			{product}
		</>
	)
}
