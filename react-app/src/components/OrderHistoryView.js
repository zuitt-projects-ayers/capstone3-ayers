import { Fragment, useState, useEffect, useContext } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function OrderHistoryView(props){

	let totalCarrier = 0
	let subtotal = 0

	const { orderData, fetchData } = props

	const [orderId, setOrderId] = useState([]);
	const [orders, setOrders] = useState([]);
	const [productPrice, setProductPrice] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);
	const [total, setTotal] = useState(0)

	useEffect(() => {

		const ordersArr = orderData.map(order => {

			console.log(order.totalAmount)
			console.log(order.productPrice)
			totalCarrier += (order.totalAmount * order.productPrice)
			subtotal = (order.totalAmount * order.productPrice)
			console.log(totalCarrier)
			console.log("stop")


			return(

				<tr key={order._id}>
					<td>{order.productName}</td>
					<td>PhP {order.productPrice}</td>
					<td className="text-center">{order.totalAmount}</td>
					<td>PhP {subtotal}</td>
					<td>{new Date().toDateString()}</td>

				</tr>
			)
		})

		setOrders(ordersArr)
		setTotal(totalCarrier)

	}, [orderData, fetchData])

	return(
		<Fragment>
			<h1 className="text-center mb-3">Order History</h1>
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Subtotal</th>
						<th>Date Purchased</th>
					</tr>					
				</thead>
				<tbody>
					{orders}
				</tbody>
	
			</Table>
		</Fragment>
	)
}