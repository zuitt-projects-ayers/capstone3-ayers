import { Row, Col, Button, Container } from 'react-bootstrap';
import { Navigate, Link, useNavigate } from 'react-router-dom';

export default function Banner(){

	const navigate = useNavigate()

	return(
		<Row>
			<Col className = "px-5 pt-5">
				<h1 className="text-center">The Classic Gamer</h1>
				<h4 className="text-center mb-3">For the discerning game enthusiast.</h4>
				{/*<Button variant="primary" className="w-100" onClick={() => navigate("/products")}>Browse Now!</Button>*/}
			</Col>
		</Row>
	)
};