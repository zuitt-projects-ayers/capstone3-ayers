const express = require('express');
const router = express.Router();
const productController = require('../controllers/productControllers');
const auth = require('../auth')

// Create Product
router.post('/create', auth.verify, (req, res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.createProduct(data).then(resultFromController => res.send(resultFromController))
});

// Retrieve All Products
router.get('/all', auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)

	productController.getAllProducts(data).then(resultFromController => res.send(resultFromController))
});

// Retrieve Active Products
router.get('/', (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// Retrieve Specific Product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Update a Product
router.put('/:productId/update', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization),
		updatedProduct : req.body
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))
});

// Archive a Product
router.put('/:productId/archive', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization)
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});

// Activate a Product
router.put('/:productId/activate', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization)
	}

	productController.activateProduct(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;