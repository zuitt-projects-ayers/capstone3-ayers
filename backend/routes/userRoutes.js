const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers')
const auth = require('../auth');

// Checking Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Register User
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Login User
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Retrieve specific details

router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
		
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Order
router.post("/order", auth.verify, (req, res) => {

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		productId : req.body.productId,
		totalAmount : req.body.totalAmount,
		productName : req.body.productName,
		productPrice : req.body.productPrice
	}

	userController.order(data).then(resultFromController => res.send(resultFromController))
})

// Retrieve Orders
router.get("/getOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
		
	userController.getOrders({userId : userData.id}).then(resultFromController => res.send(resultFromController));
})

// Retrieve Purchased Orders
router.get("/getPurchasedOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
		
	userController.getPurchasedOrders({userId : userData.id}).then(resultFromController => res.send(resultFromController));
})

module.exports = router;

// Cancel an Order
router.put('/:orderId/cancel', auth.verify, (req, res) => {

	const data = {
		orderId : req.params.orderId,
	}

	userController.cancelOrder(data).then(resultFromController => res.send(resultFromController))
});

// Purchase an Order
router.put('/:orderId/purchase', auth.verify, (req, res) => {

	const data = {
		orderId : req.params.orderId,
	}

	userController.purchaseOrder(data).then(resultFromController => res.send(resultFromController))
});