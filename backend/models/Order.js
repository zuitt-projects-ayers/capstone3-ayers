// import mongoose
const mongoose = require('mongoose');

// create data model
let userSchema = new mongoose.Schema({

	userId : {
		type : String,
		required : [true, "User ID is required."]
	},

	productId : {
		type : String,
		required : [true, "Product ID is required."]
	},

	totalAmount : {
		type : Number,
		required : [true, "Amount ordered is required."]
	},

	productName : {
		type : String,
		required : [true, "Product Name is required."]
	},

	productPrice : {
		type : Number,
		required : [true, "Product Price is required."]
	},

	purchasedOn : {
		type : Date,
		default : new Date()
	},

	orderStatus : {
		type : String,
		default : "inCart"
	}
});

// export
module.exports = mongoose.model("Order", userSchema);