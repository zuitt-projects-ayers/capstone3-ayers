const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcryptjs');
const auth = require('../auth')

// Check if email exists
module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){

			return true

		} else {
			 return false
		}
	})
};

// Registration
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err){

			 return false

		} else {

			return true
		}
	})
};

// Login
module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result)}
			} else {

				return false
			}
		}
	})
};

// Get profile
module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

// Order
module.exports.order = async (data) => {
console.log(data)
	if(data.isAdmin === true){

		 return false

	} else {
		let newOrder = new Order({
			userId : data.userId,
			productId : data.productId,
			totalAmount : data.totalAmount,
			productName : data.productName,
			productPrice : data.productPrice
		})

		return newOrder.save().then((user, err) => {

			if(err){

				 return false

			} else {

				return true
			}
		})
	}
};

// 		let isUserUpdated = await User.findById(data.userId).then(user => {

// 			user.orders.push({productId : data.productId})
			
// 			return user.save().then((user, err) => {

// 				if(err){

// 					return false

// 				} else {

// 					return true
// 				}
// 			})
// 		})

// 		let isProductUpdated = await Product.findById(data.productId).then(product => {

// 			product.orders.push({userId : data.userId})

// 			return product.save().then((product, err) => {

// 				if(err){

// 					 return false

// 				} else {

// 					return true
// 				}
// 			})
// 		})

// 		if(isUserUpdated && isProductUpdated){

// 			return true

// 		} else {

// 			return false
// 		}
// 	}
// }

// Retrieve Orders
module.exports.getOrders = (data) => {

	return Order.find({userId : data.userId, orderStatus : "inCart"}).then(result => result)
}

// Retrieve Purchased Orders
module.exports.getPurchasedOrders = (data) => {

	return Order.find({userId : data.userId, orderStatus : "Purchased"}).then(result => result)
}

// Cancel an Order
module.exports.cancelOrder = async (data) => {

		return Order.findById(data.orderId).then((result, err) => {

			result.orderStatus = "Cancelled";

			return result.save().then((cancelledOrder, err) => {

				if(err) {

					return false;

				} else {

					return true;
				}
			})
		})
}

// Purchase an Order
module.exports.purchaseOrder = async (data) => {

		return Order.findById(data.orderId).then((result, err) => {

			result.orderStatus = "Purchased";

			return result.save().then((purchasedOrder, err) => {

				if(err) {

					return false;

				} else {

					return true;
				}
			})
		})
}
