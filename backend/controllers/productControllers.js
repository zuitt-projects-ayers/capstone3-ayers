const Product = require('../models/Product');

// Add a course

// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	return newCourse.save().then((course, err) => {

// 		if(err){
// 			return false;
// 		} else {
// 			return course;
// 		}
// 	})
// }



// Create Product (Admin Only)
module.exports.createProduct = async (data) => {
console.log(data)

	if (data.isAdmin === true) {

		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price,
			releaseDate : data.product.releaseDate
		});

		return newProduct.save().then((product, error) => {

			if (error) {

				return false;

			} else {

				return true;

			};

		});

	} else {
		return false;
	};
	
};



// // Create Product (Admin Only)
// module.exports.createProduct = (req, res) => {

// 	// log info to check
// 	console.log(req.body);

// 	// check if name is already taken
// 	Product.findOne({name : req.body.name})
// 	.then(result => {

// 		if (result !== null && result.name === req.body.name) {
// 			console.log("Product Name has already been used.")
// 			return res.send('Product Name has already been used.');

// 		} else {
// 			// create new product
// 			if (req.body.publishedOn !== undefined) {
// 				let newProduct = new Product({
// 					name : req.body.name,
// 					description : req.body.description,
// 					price : req.body.price,
// 					publishedOn : new Date(req.body.publishedOn)
// 				});

// 				newProduct.save()
// 				.then(product => res.send(product))
// 				.catch(err => res.send(err));

// 			} else {
// 				let newProduct = new Product({
// 					name : req.body.name,
// 					description : req.body.description,
// 					price : req.body.price
// 				});

// 				newProduct.save()
// 				.then(product => res.send(product))
// 				.catch(err => res.send(err));
// 			}
// 		};
// 	})
// 	.catch(err => res.send(err));
// };



// Retrieve All Products
module.exports.getAllProducts = async (user) => {

	if(user.isAdmin === true){

		return Product.find({}).then(result => {

			return result
		})

	} else {

		return `${user.email} is not authorized`
	}
}

// Retrieve Active Products
module.exports.getAllActive = () => {
	
	return Product.find({isActive : true}).then(result => {

		return result
	})
}

// Retrieve Specific Product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		return result
	})
}



// Update a Product
module.exports.updateProduct = (data) => {
	console.log(data);
	return Product.findById(data.productId).then((result,err) => {
		console.log(result)
		if(data.payload.isAdmin === true){
		
				result.name = data.updatedProduct.name
				result.description = data.updatedProduct.description
				result.price = data.updatedProduct.price
				result.releaseDate = data.updatedProduct.releaseDate
				
			console.log(result)
			return result.save().then((updatedProduct, err) => {
		
				if(err){
					return false
				} else {
					return true
				}
			})
		} else {
			return false
		}
	})
}

// Archive a Product
module.exports.archiveProduct = async (data) => {

	if(data.payload.isAdmin === true) {

		return Product.findById(data.productId).then((result, err) => {

			result.isActive = false;

			return result.save().then((archivedProduct, err) => {

				if(err) {

					return false;

				} else {

					return true;
				}
			})
		})

	} else {

		return false;
	}

}

// Activate a Product
module.exports.activateProduct = async (data) => {

	if(data.payload.isAdmin === true) {

		return Product.findById(data.productId).then((result, err) => {

			result.isActive = true;

			return result.save().then((activatedProduct, err) => {

				if(err) {

					return false;

				} else {

					return true;
				}
			})
		})

	} else {

		return false;
	}
}